package com.suntak.logging;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** 
 * @Package com.suntak.log 
 * @Description: log interface
 * @date 2018年2月4日 下午7:43:55 
 * @author <a href="mailto:android_li@sina.cn">Joe</a>
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})    
@Retention(RetentionPolicy.RUNTIME)    
@Documented  
public @interface Log {
}
