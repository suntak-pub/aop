package com.szmengran.logging.service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @Package com.szmengran.logging.service
 * @Description: 云平台性能日志
 * @date 2018年3月26日 下午4:50:54
 * @author <a href="mailto:android_li@sina.cn">Joe</a>
 */
@Aspect
@Component
public class CloudPerformanceLog {
    private final static Logger logger = LoggerFactory.getLogger(CloudPerformanceLog.class);
    private final static ExecutorService executor = Executors.newCachedThreadPool();
    /**
     * Controller level pointcut Frist * represent all the type of return Second *
     * represent all class Third * represent all the method of class Last ..
     * represent all arguments
     * 
     * @return: void
     * @throws @author
     *             <a href="mailto:android_li@sina.cn">Joe</a>
     */
    @Pointcut("execution (* com..*.controller..*(..))")
    public void controllerAspect() {
    }

    /**
     * get real ip of the request
     * 
     * @param request
     * @return
     * @return: String
     * @throws @author
     *             <a href="mailto:android_li@sina.cn">Joe</a>
     */
    private String getRemortIP(HttpServletRequest request) {
        if (request.getHeader("x-forwarded-for") == null) {
            return request.getRemoteAddr();
        }
        return request.getHeader("x-forwarded-for");
    }

    /**
     * around notification
     * 
     * @param pjp
     * @return
     * @throws Throwable
     * @return: Object
     * @throws @author
     *             <a href="mailto:android_li@sina.cn">Joe</a>
     */
    @Around("controllerAspect()")
    public Object monitorAround(ProceedingJoinPoint pjp) throws Throwable {
        final Long startTime = System.currentTimeMillis();
        Object object = null;
        try {
            object = pjp.proceed();
        } catch (Exception e) {
            throw e;
        }
        
        final HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                .getRequest();
        executor.submit(() -> {
            String ip = getRemortIP(request);
            String requestUrl = getRequestUrl(request);
            String targetName = pjp.getTarget().getClass().getName();
            String methodName = pjp.getSignature().getName();
            Long spendTime = System.currentTimeMillis() - startTime;
            if (spendTime > 1000) {
                logger.warn("performance monitor log, request url:{}, method:{}.{}(),spend time:{}ms,request ip:{}", requestUrl, targetName, methodName, spendTime, ip);
            } else {
                logger.info("performance monitor log, request url:{}, method:{}.{}(),spend time:{}ms,request ip:{}", requestUrl, targetName, methodName, spendTime, ip);
            }
        });
        return object;
    }
    
    public final String getRequestUrl(HttpServletRequest request) {
        StringBuilder requestUrl = new StringBuilder();
        requestUrl.append(request.getScheme())  //当前链接使用的协议
                  .append("://")
                  .append(request.getServerName()) //服务器地址
                  .append(":")
                  .append(request.getServerPort()) //端口号
                  .append(request.getContextPath()) //应用名称，如果应用名称为
                  .append(request.getServletPath()) //请求的相对url
                  .append("?")
                  .append(request.getQueryString()); //请求参数
        return requestUrl.toString();
    }
    
    /**
     * Use to monitor exception logging
     * 
     * @param joinPoint
     * @param e
     * @return: void
     * @throws @author
     *             <a href="mailto:android_li@sina.cn">Joe</a>
     */
    @AfterThrowing(pointcut = "controllerAspect()", throwing = "e")
    public void doAfterThrowing(JoinPoint joinPoint, Throwable e) {
        final HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                .getRequest();
        executor.submit(() -> {
            String ip = getRemortIP(request);
            String requestUrl = getRequestUrl(request);
            String targetName = joinPoint.getTarget().getClass().getName();
            String methodName = joinPoint.getSignature().getName();
            logger.error("exception log:{},request url:{}, method:{}.{}(),request ip:{}", e.getMessage(), requestUrl, targetName,
                    methodName, ip);
        });
    }
}
