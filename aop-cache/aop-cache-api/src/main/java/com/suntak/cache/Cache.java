package com.suntak.cache;
/** 
 * @Package com.suntak.cache 
 * @Description: cache interface
 * @date 2018年2月8日 下午3:41:50 
 * @author <a href="mailto:android_li@sina.cn">Joe</a>
 */
public @interface Cache {
    /**
     * cache expire time
     * @return      
     * @return: String      
     * @throws   
     * @author <a href="mailto:android_li@sina.cn">Joe</a>
     */
    public String expire() default "0";    
    /**
     * cache key
     * @return      
     * @return: String      
     * @throws   
     * @author <a href="mailto:android_li@sina.cn">Joe</a>
     */
    public String key() default "";

}
