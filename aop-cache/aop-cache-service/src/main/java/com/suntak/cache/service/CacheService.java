package com.suntak.cache.service;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.suntak.cache.Cache;

/** 
 * @Package com.suntak.cache.service 
 * @Description: TODO
 * @date 2018年2月8日 下午3:41:06 
 * @author <a href="mailto:android_li@sina.cn">Joe</a>
 */
public class CacheService {

    /**
     * Controller level pointcut Frist * represent all the type of return Second *
     * represent all class Third * represent all the method of class Last ..
     * represent all arguments
     * 
     * @return: void
     * @throws @author
     *             <a href="mailto:android_li@sina.cn">Joe</a>
     */
    @Pointcut("execution (* com.suntak.*.consumer..*.*(..))")
    public void controllerAspect() {
    }

    /**
     * get real ip of the request
     * 
     * @param request
     * @return
     * @return: String
     * @throws @author
     *             <a href="mailto:android_li@sina.cn">Joe</a>
     */
    private String getRemortIP(HttpServletRequest request) {
        if (request.getHeader("x-forwarded-for") == null) {
            return request.getRemoteAddr();
        }
        return request.getHeader("x-forwarded-for");
    }

    /**
     * before notification
     * 
     * @param pjp
     * @return
     * @throws Throwable
     * @return: Object
     * @throws @author
     *             <a href="mailto:android_li@sina.cn">Joe</a>
     */
    @Before("controllerAspect()")
    public Object monitorAround(ProceedingJoinPoint pjp) throws Throwable {
        Long startTime = System.currentTimeMillis();
        Object object = null;
        try {
            object = pjp.proceed();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                .getRequest();
        String ip = getRemortIP(request);

        String targetName = pjp.getTarget().getClass().getName();
        String methodName = pjp.getSignature().getName();
        Object[] arguments = pjp.getArgs();
        Class<?> targetClass = Class.forName(targetName);
        Method[] methods = targetClass.getMethods();
        String expire = "";
        String key = "";
        for (Method method : methods) {
            if (method.getName().equals(methodName)) {
                Class<?>[] clazzs = method.getParameterTypes();
                if (clazzs.length == arguments.length) {
                    if (method.getAnnotation(Cache.class) != null) {
                        expire = method.getAnnotation(Cache.class).expire();
                        key = method.getAnnotation(Cache.class).key();
                        break;
                    } else {
                        break;
                    }
                }
            }
        }
        return object;
    }

    /**
     * 将异常消息合并为一行，写入日志里面
     * 
     * @param e
     * @return
     * @return: String
     * @throws @author
     *             <a href="mailto:android_li@sina.cn">Joe</a>
     */
    private String getAllExceptionInfor(Throwable e) {
        StringBuffer sb = new StringBuffer();
        StackTraceElement[] trace = e.getStackTrace();
        for (StackTraceElement element : trace) {
            sb.append(element).append(" ");
        }
        return sb.toString();
    }
}
