package com.suntak.exception.model;

import java.io.Serializable;


/** 
 * @Package com.suntak.exception.model 
 * @Description: 响应信息
 * @date 2018年2月12日 上午9:15:53 
 * @author <a href="mailto:android_li@sina.cn">Joe</a>
 */
public class Response implements Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Integer status;
    private Object data;
    private String message;
    public Response() {
        this.status = 200; //default value is 200, represent request success!
    }
    public Response(Integer status, String message, Object data) {
        super();
        this.status = status;
        this.message = message;
        this.data = data;
    }
    public Integer getStatus() {
        return status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }
    public Object getData() {
        return data;
    }
    public void setData(Object data) {
        this.data = data;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    
}
