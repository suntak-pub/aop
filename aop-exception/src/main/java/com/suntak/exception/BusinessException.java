package com.suntak.exception;

import com.suntak.exception.intf.IExceptionEnum;

public class BusinessException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = -7067808339181342067L;
    
    private Integer code;  //错误码
    
    public BusinessException() {}
    
    public BusinessException(IExceptionEnum iExceptionEnum) {
        super(iExceptionEnum.getMsg());
        this.code = iExceptionEnum.getCode();
    }
    
    public BusinessException(Integer code, String msg) {
        super(msg);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

}
