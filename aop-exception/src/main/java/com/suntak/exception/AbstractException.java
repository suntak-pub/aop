package com.suntak.exception;

/** 
 * @Package com.suntak.exception 
 * @Description: 业务异常接口
 * @date 2018年2月20日 上午10:33:50 
 * @author <a href="mailto:android_li@sina.cn">Joe</a>
 */
public abstract class AbstractException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Integer status;
    private String msg;
    
    public AbstractException(String msg) {
        super(msg);
        this.msg = msg;
    }
    
    public Integer getStatus() {
        return status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }
    
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }

}
