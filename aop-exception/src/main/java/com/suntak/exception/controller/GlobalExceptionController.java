package com.suntak.exception.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.suntak.exception.AbstractException;
import com.suntak.exception.BusinessException;
import com.suntak.exception.model.CommonExceptionMessage;
import com.suntak.exception.model.Response;

/**
 * @Package com.suntak.exception
 * @Description: 全局异常处理
 * @date 2018年2月12日 上午9:10:01
 * @author <a href="mailto:android_li@sina.cn">Joe</a>
 */
@RestControllerAdvice
public class GlobalExceptionController {

    private final static Logger logger = LoggerFactory.getLogger(GlobalExceptionController.class);
    //运行时异常
    @ExceptionHandler(RuntimeException.class)  
    public Response runtimeExceptionHandler(RuntimeException ex) {  
        logger.error("RuntimeException:", ex);
        return CommonExceptionMessage.retParam(1000, ex.getMessage(), null);
    }  

    //空指针异常
    @ExceptionHandler(NullPointerException.class)  
    public Response nullPointerExceptionHandler(NullPointerException ex) {  
        logger.error("NullPointerException:", ex);
        return CommonExceptionMessage.retParam(1001, ex.getMessage(), null);
    }   
    
    //类型转换异常
    @ExceptionHandler(ClassCastException.class)  
    public Response classCastExceptionHandler(ClassCastException ex) {  
        logger.error("ClassCastException:", ex);
        return CommonExceptionMessage.retParam(1002, ex.getMessage(), null);  
    }  

    //IO异常
    @ExceptionHandler(IOException.class)  
    public Response iOExceptionHandler(IOException ex) {  
        logger.error("IOException:", ex);
        return CommonExceptionMessage.retParam(1003, ex.getMessage(), null); 
    }  
    
    //未知方法异常
    @ExceptionHandler(NoSuchMethodException.class)  
    public Response noSuchMethodExceptionHandler(NoSuchMethodException ex) {  
        logger.error("NoSuchMethodException:", ex);
        return CommonExceptionMessage.retParam(1004, ex.getMessage(), null);
    }  

    //数组越界异常
    @ExceptionHandler(IndexOutOfBoundsException.class)  
    public Response indexOutOfBoundsExceptionHandler(IndexOutOfBoundsException ex) {  
        logger.error("IndexOutOfBoundsException:", ex);
        return CommonExceptionMessage.retParam(1005, ex.getMessage(), null);
    }
    
    //400错误
    @ExceptionHandler({HttpMessageNotReadableException.class})
    public Response requestNotReadable(HttpMessageNotReadableException ex){
        logger.error("HttpMessageNotReadableException:", ex);
        return CommonExceptionMessage.retParam(400, ex.getMessage(), null);
    }
    
    //400错误
    @ExceptionHandler({TypeMismatchException.class})
    public Response requestTypeMismatch(TypeMismatchException ex){
        logger.error("TypeMismatchException:", ex);
        return CommonExceptionMessage.retParam(400, ex.getMessage(), null);
    }
    
    //400错误
    @ExceptionHandler({MissingServletRequestParameterException.class})
    public Response requestMissingServletRequest(MissingServletRequestParameterException ex){
        logger.error("MissingServletRequestParameterException:", ex);
        return CommonExceptionMessage.retParam(400, ex.getMessage(), null);
    }
    
    //405错误
    @ExceptionHandler({HttpRequestMethodNotSupportedException.class})
    public Response request405(){
        return CommonExceptionMessage.retParam(405, null);
    }
    
    //406错误
    @ExceptionHandler({HttpMediaTypeNotAcceptableException.class})
    public Response request406(){
        return CommonExceptionMessage.retParam(406, null);
    }
    
    //500错误
    @ExceptionHandler({ConversionNotSupportedException.class,HttpMessageNotWritableException.class})
    public Response server500(RuntimeException ex){
        logger.error("RuntimeException:", ex);
        return CommonExceptionMessage.retParam(406, ex.getMessage(), null);
    }
    
    @ExceptionHandler({AbstractException.class})
    public Response businessException(AbstractException ex) {
        logger.error("BusinessException:", ex);
        return CommonExceptionMessage.retParam(ex.getStatus(), ex.getMsg(), null);
    }

    @ExceptionHandler({BusinessException.class})
    public Response businessException(BusinessException ex) {
        logger.error("BusinessException:", ex);
        return CommonExceptionMessage.retParam(ex.getCode(), ex.getMessage(), null);
    }
}
